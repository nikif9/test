define([ 'jquery' ], function ( $ ) { 
    return function () {
        "use strict";
        // методы в обьекте Widget доступны только в каллбэках
        const Widget = this;
        let WIDGET_CODE;

        const widgetCallbacks = this.callbacks = {};
        widgetCallbacks.bind_actions = widgetCallbacks.onSave = 
        widgetCallbacks.destroy = widgetCallbacks.init = () => true; // все коллбэки которые не используются просто возвращают true

        widgetCallbacks.render = function () {
            WIDGET_CODE = Widget.get_settings().widget_code; // т.к render всегда вызывается раньше всех остальных каллбэков то в нем записываем WIDGET_CODE для удобного обращения во всем скрипте 

            // тут должен быть ваш код проверяющий что для текущего пользователя нужно запретить закрывать задачу без результата
            // и сама логика запрета
            let task_result_value = document.getElementsByClassName("card-task__result-wrapper__inner__textarea js-task-result-textarea")
                setInterval(function() {
                    if(AMOCRM.constant("user").name == Widget.get_settings().name){
                        if (task_result_value.length >= 1) {
                            if (task_result_value.result.value.length >= 3) {
                                document.getElementsByClassName("card-task__button")[0].className = 'button-input button-input_blue js-task-result-button card-task__button'
                            } else{
                                document.getElementsByClassName("card-task__button")[0].className = "button-input card-task__button button-input-disabled"
                            }        
                }}}, 500);           

            return true; // если вернется значение == false то следующие каллбэки не вызываются
        };

        widgetCallbacks.settings = function () {
            let settingsHiddenInput = document.getElementById( WIDGET_CODE + '_custom_content' );
            // тут должен быть код отрисовывающий настройки пользователей для которых логика виджета работает
            var data = Widget.render({
                ref: '/tmpl/controls/select.twig'},// объект data в данном случае содержит только ссылку на шаблон
            {
                items: getAmoUsers(),      //данные
            });
            save_test_widgtes.onclick = function() {
                if (document.getElementById('save_test_widgtes').className == "button-input   js-widget-save button-input_blue"){
                    let control_select = document.getElementsByClassName("control--select--button-inner")[0].textContent.toString()
                    alert(control_select);
                    Widget.set_settings({name:control_select}); //создается свойство с именем par1 и значением text
                }                
            };
            settingsHiddenInput.innerHTML = data;
            return true;
        };
        return this; // внутри амо виджет вызывается через new поэтому функция возырвщвет this с каллбэками.

        // получить настройки виджета
        function getSettings() {
            let settings = Widget.get_settings().config;
            if (typeof settings === "string") settings = JSON.parse(settings);

            return settings;
        }

        // получить текущую область
        function getArea() {
            return Widget.system().area;
        }

        // получить пользователей amocrm 
        function getAmoUsers() {
            return AMOCRM.constant("managers");
        }
    }

})